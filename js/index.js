let number = 0;

while (number < 10) {
    console.log(number);
    number++;
};

if (number === 10) {
    console.log("plein");
}

else if (number > 10) {
    console.log("a vider");
}

else {
    console.log("a remplir");
};

if (number >= -1 && number <= 1 || number === 10) {
    console.log(true);
}

else {
    console.log(false);
}